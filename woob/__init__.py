# -*- coding: utf-8 -*-

import pkgutil

__path__ = pkgutil.extend_path(__path__, __name__)

__title__ = 'woob'
__version__ = '3.0'
__author__ = 'The Woob Association'
__copyright__ = 'Copyright 2012-2019 The Woob Association'
