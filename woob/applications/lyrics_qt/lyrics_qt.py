# -*- coding: utf-8 -*-

# Copyright(C) 2016 Julien Veyssier
#
# This file is part of woob.
#
# woob is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# woob is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with woob. If not, see <http://www.gnu.org/licenses/>.


from woob.capabilities.lyrics import CapLyrics
from woob.tools.application.qt5 import QtApplication

from .main_window import MainWindow


class QAppLyrics(QtApplication):
    APPNAME = 'lyrics_qt'
    VERSION = '3.0'
    COPYRIGHT = 'Copyright(C) 2016 Julien Veyssier'
    DESCRIPTION = "Qt application allowing to search song lyrics."
    SHORT_DESCRIPTION = "search lyrics"
    CAPS = CapLyrics
    CONFIG = {'settings': {'backend': '',
                           'maxresultsnumber': '10'
                           }
              }

    def main(self, argv):
        self.load_backends([CapLyrics])
        self.load_config()

        main_window = MainWindow(self.config, self.woob, self)
        main_window.show()
        return self.woob.loop()
