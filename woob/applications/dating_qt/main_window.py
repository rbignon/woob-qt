# -*- coding: utf-8 -*-

# Copyright(C) 2010-2014 Romain Bignon
#
# This file is part of woob.
#
# woob is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# woob is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with woob. If not, see <http://www.gnu.org/licenses/>.

from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSlot as Slot

from woob.tools.application.qt5 import QtMainWindow
from woob.tools.application.qt5.backendcfg import BackendCfg
from woob.capabilities.dating import CapDating

try:
    from woob.applications.msg_qt.messages_manager import MessagesManager
    HAVE_WOOBMSG = True
except ImportError:
    HAVE_WOOBMSG = False

from .ui.main_window_ui import Ui_MainWindow
from .status import AccountsStatus
from .contacts import ContactsWidget
from .events import EventsWidget
from .search import SearchWidget


class MainWindow(QtMainWindow):
    def __init__(self, config, woob, parent=None):
        super(MainWindow, self).__init__(parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.config = config
        self.woob = woob

        self.loaded_tabs = {}

        self.ui.actionBackends.triggered.connect(self.backendsConfig)
        self.ui.tabWidget.currentChanged.connect(self.tabChanged)

        self.addTab(AccountsStatus(self.woob), self.tr('Status'))
        self.addTab(MessagesManager(self.woob) if HAVE_WOOBMSG else None, self.tr('Messages'))
        self.addTab(ContactsWidget(self.woob), self.tr('Contacts'))
        self.addTab(EventsWidget(self.woob), self.tr('Events'))
        self.addTab(SearchWidget(self.woob), self.tr('Search'))
        self.addTab(None, self.tr('Calendar'))
        self.addTab(None, self.tr('Optimizations'))

        if self.woob.count_backends() == 0:
            self.backendsConfig()

    @Slot()
    def backendsConfig(self):
        bckndcfg = BackendCfg(self.woob, (CapDating,), self)
        if bckndcfg.run():
            self.loaded_tabs.clear()
            widget = self.ui.tabWidget.widget(self.ui.tabWidget.currentIndex())
            widget.load()

    def addTab(self, widget, title):
        if widget:
            if getattr(widget, 'display_contact', None):
                widget.display_contact.connect(self.display_contact)
            self.ui.tabWidget.addTab(widget, title)
        else:
            index = self.ui.tabWidget.addTab(QWidget(), title)
            self.ui.tabWidget.setTabEnabled(index, False)

    @Slot(int)
    def tabChanged(self, i):
        widget = self.ui.tabWidget.currentWidget()

        if hasattr(widget, 'load') and i not in self.loaded_tabs:
            widget.load()
            self.loaded_tabs[i] = True

    @Slot(object)
    def display_contact(self, contact):
        self.ui.tabWidget.setCurrentIndex(2)
        widget = self.ui.tabWidget.currentWidget()
        widget.setContact(contact)
