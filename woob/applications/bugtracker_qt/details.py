# -*- coding: utf-8 -*-

# Copyright(C) 2013 Sébastien Monel
#
# This file is part of woob.
#
# woob is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# woob is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with woob. If not, see <http://www.gnu.org/licenses/>.

from xml.sax.saxutils import escape as escape_html

from PyQt5.QtWidgets import QDialog
from woob.capabilities.base import empty

from .ui.details_ui import Ui_Dialog


def esc(o):
    return escape_html(str(o))


class DetailDialog(QDialog):
    def __init__(self, issue, parent=None):
        super(DetailDialog, self).__init__(parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        self.issue = issue

        self.ui.titleEdit.setText(issue.title or '')
        self.ui.authorEdit.setText(issue.author.name if issue.author else '')
        self.ui.assigneeEdit.setText(issue.assignee.name if issue.assignee else '')
        self.ui.creationEdit.setText(str(issue.creation))
        self.ui.lastEdit.setText(str(issue.updated))

        url = ''
        if issue.url:
            url = '<a href="%s">%s</a>' % (issue.url, issue.url)
        self.ui.linkLabel.setText(url)

        self.ui.bodyEdit.setPlainText(str(issue.body))
        self.ui.updatesEdit.setHtml(self.updatesToHtml(issue.history))

    def updatesToHtml(self, updates):
        if empty(updates):
            return ''

        parts = []
        for update in updates:
            parts.append('<b>%s</b> %s' % (update.date, esc(update.author and update.author.name)))
            if update.message or not update.changes:
                parts.append('<pre>%s</pre>' % esc(update.message))
            parts.extend(self.changesToHtml(update.changes))
            parts.append('<hr/>')
        return '\n'.join(parts)

    def changesToHtml(self, changes):
        if not changes:
            return

        yield '<ul>'
        for change in changes:
            yield '<li><i>%s</i> changed to <i><ins>%s</ins></i> (from <i><del>%s</del></i>)</li>' % (esc(change.field), esc(change.new), esc(change.last))
        yield '</ul>'

