core := woob/tools/application/qt5
applications := msg_qt dating_qt webcontentedit_qt housing_qt cinema_qt recipes_qt job_qt lyrics_qt gallery_qt bugtracker_qt
ifeq ($(WIN32),)
	applications += video_qt
endif

directories := $(core) $(applications:%=woob/applications/%/ui)

.PHONY: clean all $(directories)

all: target := all
all: $(directories)

clean: target := clean
clean: $(directories)

$(directories):
	$(MAKE) -C $@ $(target)
